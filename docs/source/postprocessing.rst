Post-processing
===============

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Impossible velocity jumps correction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: correct_impossible_velocity_jumps
  :members:

^^^^^^^^^^^^^^^^
Assign crossings
^^^^^^^^^^^^^^^^

.. automodule:: assign_them_all
  :members:

^^^^^^^^^^^^^^^^
Get trajectories
^^^^^^^^^^^^^^^^

Produces the dictionary of outputs containing the individual trajectories (centroids),
the git output, the path to the video whose tracking poroduced the individual trajectories and
frames_per_second.

.. automodule:: get_trajectories
  :members:
